import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.alki.app',
  appName: 'Alki',
  webDir: 'dist/alki/browser',
  bundledWebRuntime: false,
  plugins: {
    SplashScreen: {
      launchAutoHide: false,
      splashFullScreen: true,
      SplashImmersive: false
    }
  }
};

export default config;
