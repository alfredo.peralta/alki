import {
  MAT_BUTTON_CONFIG,
  MAT_FAB_DEFAULT_OPTIONS,
  MAT_FAB_DEFAULT_OPTIONS_FACTORY,
  MatAnchor,
  MatButton,
  MatButtonModule,
  MatFabAnchor,
  MatFabButton,
  MatIconAnchor,
  MatIconButton,
  MatMiniFabAnchor,
  MatMiniFabButton
} from "./chunk-26YE5TSY.js";
import "./chunk-MJDW2NQU.js";
import "./chunk-NRXF7CXP.js";
import "./chunk-S4AZCMJY.js";
import "./chunk-PZQZAEDH.js";
export {
  MAT_BUTTON_CONFIG,
  MAT_FAB_DEFAULT_OPTIONS,
  MAT_FAB_DEFAULT_OPTIONS_FACTORY,
  MatAnchor,
  MatButton,
  MatButtonModule,
  MatFabAnchor,
  MatFabButton,
  MatIconAnchor,
  MatIconButton,
  MatMiniFabAnchor,
  MatMiniFabButton
};
//# sourceMappingURL=@angular_material_button.js.map
