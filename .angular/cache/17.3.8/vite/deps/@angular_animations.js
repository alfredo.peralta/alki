import {
  AUTO_STYLE,
  AnimationBuilder,
  AnimationFactory,
  AnimationGroupPlayer,
  AnimationMetadataType,
  BrowserAnimationBuilder,
  NoopAnimationPlayer,
  animate,
  animateChild,
  animation,
  group,
  keyframes,
  query,
  sequence,
  stagger,
  state,
  style,
  transition,
  trigger,
  useAnimation,
  ɵPRE_STYLE
} from "./chunk-G3LANL3Y.js";
import "./chunk-NRXF7CXP.js";
import "./chunk-S4AZCMJY.js";
import "./chunk-PZQZAEDH.js";
export {
  AUTO_STYLE,
  AnimationBuilder,
  AnimationFactory,
  AnimationMetadataType,
  NoopAnimationPlayer,
  animate,
  animateChild,
  animation,
  group,
  keyframes,
  query,
  sequence,
  stagger,
  state,
  style,
  transition,
  trigger,
  useAnimation,
  AnimationGroupPlayer as ɵAnimationGroupPlayer,
  BrowserAnimationBuilder as ɵBrowserAnimationBuilder,
  ɵPRE_STYLE
};
//# sourceMappingURL=@angular_animations.js.map
