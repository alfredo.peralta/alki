import {
  Directory,
  Encoding,
  FilesystemDirectory,
  FilesystemEncoding
} from "./chunk-3UJUPE3K.js";
import {
  registerPlugin
} from "./chunk-QUGK7XI7.js";
import "./chunk-PZQZAEDH.js";

// node_modules/@capacitor/filesystem/dist/esm/index.js
var Filesystem = registerPlugin("Filesystem", {
  web: () => import("./web-6PEOHY5V.js").then((m) => new m.FilesystemWeb())
});
export {
  Directory,
  Encoding,
  Filesystem,
  FilesystemDirectory,
  FilesystemEncoding
};
//# sourceMappingURL=@capacitor_filesystem.js.map
