import {
  registerPlugin
} from "./chunk-QUGK7XI7.js";
import "./chunk-PZQZAEDH.js";

// node_modules/@capacitor/share/dist/esm/index.js
var Share = registerPlugin("Share", {
  web: () => import("./web-TTPAT3CR.js").then((m) => new m.ShareWeb())
});
export {
  Share
};
//# sourceMappingURL=@capacitor_share.js.map
