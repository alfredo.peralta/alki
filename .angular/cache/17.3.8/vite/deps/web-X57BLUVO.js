import {
  CapacitorException,
  ExceptionCode,
  WebPlugin
} from "./chunk-QUGK7XI7.js";
import {
  __async
} from "./chunk-PZQZAEDH.js";

// node_modules/@capacitor-mlkit/barcode-scanning/dist/esm/web.js
var BarcodeScannerWeb = class extends WebPlugin {
  startScan(_options) {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  stopScan() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  readBarcodesFromImage(_options) {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  scan() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  isSupported() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  enableTorch() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  disableTorch() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  toggleTorch() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  isTorchEnabled() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  isTorchAvailable() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  setZoomRatio(_options) {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  getZoomRatio() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  getMinZoomRatio() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  getMaxZoomRatio() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  openSettings() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  isGoogleBarcodeScannerModuleAvailable() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  installGoogleBarcodeScannerModule() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  checkPermissions() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  requestPermissions() {
    return __async(this, null, function* () {
      throw this.createUnavailableException();
    });
  }
  createUnavailableException() {
    return new CapacitorException("This Barcode Scanner plugin method is not available on this platform.", ExceptionCode.Unavailable);
  }
};
export {
  BarcodeScannerWeb
};
//# sourceMappingURL=web-X57BLUVO.js.map
