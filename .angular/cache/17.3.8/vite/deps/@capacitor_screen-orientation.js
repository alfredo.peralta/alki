import {
  registerPlugin
} from "./chunk-QUGK7XI7.js";
import "./chunk-PZQZAEDH.js";

// node_modules/@capacitor/screen-orientation/dist/esm/index.js
var ScreenOrientation = registerPlugin("ScreenOrientation", {
  web: () => import("./web-2JHFOO6D.js").then((m) => new m.ScreenOrientationWeb())
});
export {
  ScreenOrientation
};
//# sourceMappingURL=@capacitor_screen-orientation.js.map
