import {
  registerPlugin
} from "./chunk-QUGK7XI7.js";
import "./chunk-PZQZAEDH.js";

// node_modules/@capacitor/network/dist/esm/index.js
var Network = registerPlugin("Network", {
  web: () => import("./web-MTZTRF3L.js").then((m) => new m.NetworkWeb())
});
export {
  Network
};
//# sourceMappingURL=@capacitor_network.js.map
