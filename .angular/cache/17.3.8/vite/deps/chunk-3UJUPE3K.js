// node_modules/@capacitor/filesystem/dist/esm/definitions.js
var Directory;
(function(Directory2) {
  Directory2["Documents"] = "DOCUMENTS";
  Directory2["Data"] = "DATA";
  Directory2["Library"] = "LIBRARY";
  Directory2["Cache"] = "CACHE";
  Directory2["External"] = "EXTERNAL";
  Directory2["ExternalStorage"] = "EXTERNAL_STORAGE";
})(Directory || (Directory = {}));
var Encoding;
(function(Encoding2) {
  Encoding2["UTF8"] = "utf8";
  Encoding2["ASCII"] = "ascii";
  Encoding2["UTF16"] = "utf16";
})(Encoding || (Encoding = {}));
var FilesystemDirectory = Directory;
var FilesystemEncoding = Encoding;

export {
  Directory,
  Encoding,
  FilesystemDirectory,
  FilesystemEncoding
};
//# sourceMappingURL=chunk-3UJUPE3K.js.map
