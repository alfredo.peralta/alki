import {
  registerPlugin
} from "./chunk-QUGK7XI7.js";
import "./chunk-PZQZAEDH.js";

// node_modules/@capacitor-mlkit/barcode-scanning/dist/esm/definitions.js
var BarcodeFormat;
(function(BarcodeFormat2) {
  BarcodeFormat2["Aztec"] = "AZTEC";
  BarcodeFormat2["Codabar"] = "CODABAR";
  BarcodeFormat2["Code39"] = "CODE_39";
  BarcodeFormat2["Code93"] = "CODE_93";
  BarcodeFormat2["Code128"] = "CODE_128";
  BarcodeFormat2["DataMatrix"] = "DATA_MATRIX";
  BarcodeFormat2["Ean8"] = "EAN_8";
  BarcodeFormat2["Ean13"] = "EAN_13";
  BarcodeFormat2["Itf"] = "ITF";
  BarcodeFormat2["Pdf417"] = "PDF_417";
  BarcodeFormat2["QrCode"] = "QR_CODE";
  BarcodeFormat2["UpcA"] = "UPC_A";
  BarcodeFormat2["UpcE"] = "UPC_E";
})(BarcodeFormat || (BarcodeFormat = {}));
var BarcodeValueType;
(function(BarcodeValueType2) {
  BarcodeValueType2["CalendarEvent"] = "CALENDAR_EVENT";
  BarcodeValueType2["ContactInfo"] = "CONTACT_INFO";
  BarcodeValueType2["DriversLicense"] = "DRIVERS_LICENSE";
  BarcodeValueType2["Email"] = "EMAIL";
  BarcodeValueType2["Geo"] = "GEO";
  BarcodeValueType2["Isbn"] = "ISBN";
  BarcodeValueType2["Phone"] = "PHONE";
  BarcodeValueType2["Product"] = "PRODUCT";
  BarcodeValueType2["Sms"] = "SMS";
  BarcodeValueType2["Text"] = "TEXT";
  BarcodeValueType2["Url"] = "URL";
  BarcodeValueType2["Wifi"] = "WIFI";
  BarcodeValueType2["Unknown"] = "UNKNOWN";
})(BarcodeValueType || (BarcodeValueType = {}));
var LensFacing;
(function(LensFacing2) {
  LensFacing2["Front"] = "FRONT";
  LensFacing2["Back"] = "BACK";
})(LensFacing || (LensFacing = {}));
var GoogleBarcodeScannerModuleInstallState;
(function(GoogleBarcodeScannerModuleInstallState2) {
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["UNKNOWN"] = 0] = "UNKNOWN";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["PENDING"] = 1] = "PENDING";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["DOWNLOADING"] = 2] = "DOWNLOADING";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["CANCELED"] = 3] = "CANCELED";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["COMPLETED"] = 4] = "COMPLETED";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["FAILED"] = 5] = "FAILED";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["INSTALLING"] = 6] = "INSTALLING";
  GoogleBarcodeScannerModuleInstallState2[GoogleBarcodeScannerModuleInstallState2["DOWNLOAD_PAUSED"] = 7] = "DOWNLOAD_PAUSED";
})(GoogleBarcodeScannerModuleInstallState || (GoogleBarcodeScannerModuleInstallState = {}));

// node_modules/@capacitor-mlkit/barcode-scanning/dist/esm/index.js
var BarcodeScanner = registerPlugin("BarcodeScanner", {
  web: () => import("./web-X57BLUVO.js").then((m) => new m.BarcodeScannerWeb())
});
export {
  BarcodeFormat,
  BarcodeScanner,
  BarcodeValueType,
  GoogleBarcodeScannerModuleInstallState,
  LensFacing
};
//# sourceMappingURL=@capacitor-mlkit_barcode-scanning.js.map
