import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../../enviroment/AppSettings';
@Injectable({
  providedIn: 'root'
})
export class PrivadaService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }


  consultaPrivadas(): Observable<any> {
    console.info('llega al servicio:'+this.URL+"/service/privada.php")
    return this.http.get<any>(this.URL+"/service/privada.php?method=listarPrivadas");
  }

}
