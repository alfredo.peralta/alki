import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../enviroment/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class AlertaService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }

  consultaAlertas(token: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token);
    return this.http.post<any>(this.URL+"/service/consultaAlertas.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  guardarAlertasEstatus(token: any, id: any, estatus: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token).set('id',id).set('estatus',estatus);
    return this.http.post<any>(this.URL+"/service/modificaEstatus.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  guardarAlerta(token: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token);
    return this.http.post<any>(this.URL+"/service/agregaAlerta.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

}
