import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../enviroment/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }
  
  init(token: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token);
    return this.http.post<any>(this.URL+"/service/home.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

}
