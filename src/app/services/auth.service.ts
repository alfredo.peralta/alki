import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../enviroment/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }

  login(user: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('correo', user.correo).set('password', user.password);
    return this.http.post<any>(this.URL+"/service/login.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  restaurarPassword(user: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('correo', user.correo);
    return this.http.post<any>(this.URL+"/service/recuperar.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  getToken(){
    return localStorage.getItem('token');
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

}
