import { Injectable } from '@angular/core';
import { Network } from '@capacitor/network';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor() {
    this.initializeNetworkListener();
  }

  // Verifica el estado inicial de la conexión
  async checkNetworkStatus() {
    const status = await Network.getStatus();
    console.log('Conectado a internet:', status.connected);
    return status.connected;
  }

  // Escucha los cambios en el estado de la red
  initializeNetworkListener() {
    Network.addListener('networkStatusChange', (status) => {
      console.log('Estado de la red cambió:', status);
    });
  }

}
