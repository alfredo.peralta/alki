import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../enviroment/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class VisitasService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }

  consultaVisitas(token: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token);
    return this.http.post<any>(this.URL+"/service/consultaVisitas.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  validaQr(acceso: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('nombre', acceso.nombre).set('cantidad', acceso.cantidad).set('acceso', acceso.acceso).set('id', acceso.id).set('fecha', acceso.fecha);
    return this.http.post<any>(this.URL+"/service/validaAcesso.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  obtenerFechaActual(){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams();
    return this.http.post<any>(this.URL+"/service/obtenerFechaQr.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  validaVigenciaQr(fecha: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('fecha', fecha);
    return this.http.post<any>(this.URL+"/service/obtenerEstatusQr.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

}
