import { TestBed } from '@angular/core/testing';

import { PrivadaService } from './privada.service';

describe('PrivadaService', () => {
  let service: PrivadaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrivadaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
