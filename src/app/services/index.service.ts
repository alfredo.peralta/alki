import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../enviroment/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  private URL  = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }

  index(token: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token);
    return this.http.post<any>(this.URL+"/service/index.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

  indexUpdate(token: any, user: any){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpParams = new HttpParams().set('token', token).set('nombre',user.nombre).set('apellidos',user.apellidos).set('correo',user.correo).set('password',user.password).set('telefono',user.telefono);
    return this.http.post<any>(this.URL+"/service/indexActualiza.php", httpParams.toString(), {
      headers: headerOptions
    });
  }

}