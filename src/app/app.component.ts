import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ResidentesComponent } from './pages/residentes/residentes.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MatButtonModule, LoginComponent, HomeComponent, ResidentesComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  
})


export class AppComponent {
  title = 'alki';
}
