import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AuthService } from '../../services/auth.service';
import { HomeService } from '../../services/home.service';
import { AlertaService } from '../../services/alerta.service';
import { ResidentesComponent } from '../residentes/residentes.component';
import { RouterModule } from '@angular/router'; 
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { NotificacionesService } from '../../services/notificaciones.service';
import { ServicioService } from '../../services/servicio.service';
import { ScreenOrientation } from '@capacitor/screen-orientation';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [ResidentesComponent, RouterModule],
  animations: [
    trigger('popOverState', [
      state('show', style({
        opacity: 1,
        display: "block"
      })),
      state('hide',   style({
        opacity: 0,
        display:"none"
      })),
      transition('show => hide', animate('300ms ease-out')),
      transition('hide => show', animate('300ms ease-in'))
    ])
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent{
  title = "";
  
  show = (this.isMobile()) ? false : true;

  live = null;

  constructor(private authService: AuthService, private homeService: HomeService, private alertaService: AlertaService, private router: Router, private notificacionesService: NotificacionesService, private servicioService : ServicioService) {
    this.initHome();
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
  }

  get stateName() {
    return this.show ? 'show' : 'hide'
  }

  toggle() {
    this.show = !this.show;
  }

  data = { correo: '', oaut: true}
  habilita = false;

  isMobile(){
  let respuesta = false;
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(navigator.userAgent)){
      respuesta = true;
    }
    return respuesta;
  }

  closedMenu(){
    if(this.isMobile()){
      if(this.show == true){
        this.show = false;
      }
    }
  }

  alerta(){
    this.validaServicio();

    Swal.fire({
      title: "¿Desea activar la alerta de pánico?",
      text: "Una vez activada no se podrá cancelar!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, enviar!",
      cancelButtonText: "Cancelar"
    }).then((result) => {
      if (result.isConfirmed) {

        this.alertaService.guardarAlerta(this.authService.getToken()).subscribe(res => {
          if(res != null){
            Swal.fire({
              title: "ALKI",
              text: res.response,
              icon: res.code
            });

            if(res.code == "success"){
              this.router.navigate(['/home/alertas']);
              this.envioSMS();
            }

          }else{
            Swal.fire({
              title: "ALKI",
              text: "No se logro solicitar, intente más tarde!",
              icon: "info"
            });
          }
        });
      }
    });
  }

  cerrarSesion(){
    this.authService.logout();
  }

  initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res.correo != null && res.oaut != null){
        this.data.correo = res.correo;
        this.data.oaut = res.oaut;
        this.habilita = this.data.oaut;
        this.live = res.live;
      }
    });
  }

  envioSMS(){
    this.notificacionesService.notificaSMS(this.authService.getToken()).subscribe(res => {});
  }

  /** valida servicio residente **/
  validaServicio(){
    this.servicioService.validaServicio(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }

}

