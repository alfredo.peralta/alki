import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { VisitasService } from '../../services/visitas.service';
import { AuthService } from '../../services/auth.service';
import { HomeService } from '../../services/home.service';
import {FormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';
import Swal from 'sweetalert2';

export class VisitaData {
  constructor(public fecha : string, public direccion: string){}
}

@Component({
  selector: 'app-visitas',
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, FormsModule, MatSelectModule],
  templateUrl: './visitas.component.html',
  styleUrl: './visitas.component.css'
})
export class VisitasComponent implements OnInit, AfterViewInit{

  visitas: VisitaData[] = [];
  displayedColumns: string[] = ['fecha', 'descripcion'];
  habilita = false;
  total = 0;
  
  dataSource: MatTableDataSource<VisitaData>;

  @ViewChild(MatPaginator) paginator: MatPaginator = new MatPaginator(new MatPaginatorIntl(), ChangeDetectorRef.prototype);
  @ViewChild(MatSort) sort: MatSort = new MatSort();

  constructor(private visitasService: VisitasService, private authService: AuthService, private homeService: HomeService, private networkService: NetworkService) {
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource<VisitaData>(this.visitas);
    this.initHome();
    /** Consulta Visitas **/
    this.consultaVisitas();
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
      if (length === 0 || pageSize === 0) {
        return `0 de ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} - ${endIndex} de ${length}`;
    };
  }

  async ngOnInit() {
    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async consultaVisitas() {
    this.visitasService.consultaVisitas(this.authService.getToken()).subscribe(res => {
      if(res!=null){
        this.dataSource.data = res;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.total = res.length;
        this.paginator._intl.itemsPerPageLabel = "Registros por página";
        this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
          if (length === 0 || pageSize === 0) {
            return `0 de ${length }`;
          }
          length = Math.max(length, 0);
          const startIndex = page * pageSize;
          // If the start index exceeds the list length, do not try and fix the end index to the end.
          const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
          return `${startIndex + 1} - ${endIndex} de ${length}`;
        };
      }
    });
  }

  ngAfterViewInit() {
    this.visitasService.consultaVisitas(this.authService.getToken()).subscribe(res => {
      if(res!=null){
        this.dataSource.data = res;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;    
        this.paginator._intl.itemsPerPageLabel = "Registros por página";
        this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
          if (length === 0 || pageSize === 0) {
            return `0 de ${length }`;
          }
          length = Math.max(length, 0);
          const startIndex = page * pageSize;
          // If the start index exceeds the list length, do not try and fix the end index to the end.
          const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
          return `${startIndex + 1} - ${endIndex} de ${length}`;
        };
      }
    });
  }

  async initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res.oaut != null){
        this.habilita = res.oaut;
      }
    });
  }

}
