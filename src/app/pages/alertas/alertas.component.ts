import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AlertaService } from '../../services/alerta.service';
import { AuthService } from '../../services/auth.service';
import { HomeService } from '../../services/home.service';
import {FormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import Swal from 'sweetalert2';
import { ServicioService } from '../../services/servicio.service';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';

export class AlertData {
  constructor(public id: string, public fecha : string, public direccion: string, public estatus: string){}
}

@Component({
  selector: 'app-alertas',
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, FormsModule, MatSelectModule],
  templateUrl: './alertas.component.html',
  styleUrl: './alertas.component.css'
})
export class AlertasComponent implements OnInit, AfterViewInit {

  alertas: AlertData[] = [];
  displayedColumns: string[] = ['id', 'fecha', 'direccion', 'estatus'];
  habilita = false;
  total = 0;
  
  dataSource: MatTableDataSource<AlertData>;

  @ViewChild(MatPaginator) paginator: MatPaginator = new MatPaginator(new MatPaginatorIntl(), ChangeDetectorRef.prototype);
  @ViewChild(MatSort) sort: MatSort = new MatSort();

  constructor(private alertaService: AlertaService, private authService: AuthService, private homeService: HomeService, private servicioService: ServicioService, private networkService: NetworkService) {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource<AlertData>(this.alertas);
    this.initHome();
    /** Consulta Alertas **/
    this.consultaAlerta();
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
      if (length === 0 || pageSize === 0) {
        return `0 de ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} - ${endIndex} de ${length}`;
    };
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
  }

  async ngOnInit(){
    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }
  }

  onChange(value: any, id: any){
    let estado = value.target.value;
    let idEstatus = id;

    this.alertaService.guardarAlertasEstatus(this.authService.getToken(), idEstatus, estado).subscribe(res => {
      if(res!=null){
        Swal.fire({
          title: 'ALKI',
          icon: res.code,
          html: res.response
        })
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async consultaAlerta() {
    this.alertaService.consultaAlertas(this.authService.getToken()).subscribe(res => {
      if(res!=null){
        this.dataSource.data = res;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.total = res.length;
        this.paginator._intl.itemsPerPageLabel = "Registros por página";
        this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
          if (length === 0 || pageSize === 0) {
            return `0 de ${length }`;
          }
          length = Math.max(length, 0);
          const startIndex = page * pageSize;
          // If the start index exceeds the list length, do not try and fix the end index to the end.
          const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
          return `${startIndex + 1} - ${endIndex} de ${length}`;
        };
      }
    });
  }

  ngAfterViewInit() {
    this.alertaService.consultaAlertas(this.authService.getToken()).subscribe(res => {
      if(res!=null){
        this.dataSource.data = res;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;    
        this.paginator._intl.itemsPerPageLabel = "Registros por página";
        this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => { 
          if (length === 0 || pageSize === 0) {
            return `0 de ${length }`;
          }
          length = Math.max(length, 0);
          const startIndex = page * pageSize;
          // If the start index exceeds the list length, do not try and fix the end index to the end.
          const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
          return `${startIndex + 1} - ${endIndex} de ${length}`;
        };
      }
    });
  }

  async initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res.oaut != null){
        this.habilita = res.oaut;
        if(!res.oaut){
          this.validaServicioSeguridad();
        }
      }
    });
  }

  validaServicioSeguridad(){
    this.servicioService.validaServicioSeguridad(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }

}