import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesoResidenteComponent } from './acceso-residente.component';

describe('AccesoResidenteComponent', () => {
  let component: AccesoResidenteComponent;
  let fixture: ComponentFixture<AccesoResidenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AccesoResidenteComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AccesoResidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
