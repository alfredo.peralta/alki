import { Component,NgZone, OnInit } from '@angular/core';
import { FormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { QrCodeModule } from 'ng-qrcode';
import Swal from 'sweetalert2';
import { NgIf } from '@angular/common';
import html2canvas from 'html2canvas';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Share } from '@capacitor/share';
import { AuthService } from '../../services/auth.service';
import { HomeService } from '../../services/home.service';
import {
  Barcode,
  BarcodeFormat,
  BarcodeScanner,
  LensFacing,
} from '@capacitor-mlkit/barcode-scanning';
import { VisitasService } from '../../services/visitas.service';
import { decrypt, encrypt } from '../../util/util-encrypt';
import { ServicioService } from '../../services/servicio.service';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';

@Component({
  selector: 'app-acceso-residente',
  standalone: true,
  imports: [FormsModule, QrCodeModule, NgIf],
  templateUrl: './acceso-residente.component.html',
  styleUrl: './acceso-residente.component.css'
})
export class AccesoResidenteComponent implements OnInit{

  acceso = {
    nombre: '',
    cantidad: '',
    token: '',
    tipo: ''
  }

  /** qr **/
  public readonly barcodeFormat = BarcodeFormat;
  public readonly lensFacing = LensFacing;

  public formGroup = new UntypedFormGroup({
    formats: new UntypedFormControl([]),
    lensFacing: new UntypedFormControl(LensFacing.Back),
    googleBarcodeScannerModuleInstallState: new UntypedFormControl(0),
    googleBarcodeScannerModuleInstallProgress: new UntypedFormControl(0),
  });
  public barcodes: Barcode[] = [];
  public isSupported = false;
  public isPermissionGranted = false;
  /** fin qr **/

  compartir = false;
  logoQr = true;
  habilita = false;
  vigencia = "";
  titleEscaner = "Escanear QR";
  idUsuario = 0;
  cantidades: any;
  tipoAcceso: any;

  availableDevices: MediaDeviceInfo[] = [];

  scanResult: any='';
  showScanner = false;
  showScanBtn = true;
  estilo = "";

  public barcodeResult!: string;

  constructor(private authService:AuthService, private homeService: HomeService,
    private readonly ngZone: NgZone, private visitasService: VisitasService, private servicioService: ServicioService, private networkService: NetworkService){
    this.initHome();
    this.vigenciaQr();
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
  }

  async validaCantidad(input: string) {
    const pattern = /^[0-9]+$/;
    if (!pattern.test(input)) {
      Swal.fire({
        title: "Cantidad no válida",
        text: "no cumple con el formato requerido",
        icon: "info"
      });
      return;
    }
  }

  async validaVisitante(input: string) {
    const pattern = /^[a-zA-ZñÑÁÉÍÓÚáéíóú ]+$/;
    if (!pattern.test(input)) {
      Swal.fire({
        title: "Nombre de visitante no válida",
        text: "no cumple con el formato requerido",
        icon: "info"
      });
      return;
    }
  }

  async ngOnInit() {
    this.cantidades = ["","1","2","3","4","5"];

    this.tipoAcceso = ["","Entrada","Salida"];

    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }

    this.installGoogleBarcodeScanner();
  }

  async installGoogleBarcodeScanner() {
    try {
      await BarcodeScanner.installGoogleBarcodeScannerModule();
      const available = await BarcodeScanner.isGoogleBarcodeScannerModuleAvailable();
        if(available){
          console.log("El módulo Google Barcode Scanner está instalado..");
        }else{
          console.log("El módulo Google Barcode Scanner no se pudo instalar.");
        }
    } catch (error) {
      console.log("Error al instalar el modulo: "+error);
    }
  }

  requestPermissions = async () => {
    await BarcodeScanner.requestPermissions();
    try {
      const { supported } = await BarcodeScanner.isSupported();
      if(supported){
        console.log("Dispositivo soportado.");
      }else{
        Swal.fire({
          title: 'ALKI',
          icon: 'error',
          html: 'El escaner no esta soportado para este dispositivo.'
        })
      } 
    } catch (error) {
      Swal.fire({
        title: 'ALKI',
        icon: 'error',
        html: 'El escaner no esta soportado: '+error
      })
    }
  };

  scanSuccessHandler(result:string){ 
    if(result!=null || result!=""){
      this.showScanner = false;
      this.showScanBtn = true;
      this.logoQr = true;
      this.titleEscaner = "Escanear QR";
      this.scanResult=result;  
      this.scanResult = atob(this.scanResult);
    }
  }

  /** Captura httml to image **/
  captureScreen(){
    const element = document.getElementById("qrImage") as HTMLElement;
    html2canvas(element).then((canvas: HTMLCanvasElement) => {
      //this.download(canvas);
      this.share(canvas);
    })
  }

  download(canvas: HTMLCanvasElement){
    const link = document.createElement("a");
    link.href = canvas.toDataURL();
    link.download = "qr.png";
    link.click();
  }

   scanBarcode = async () => {
    this.showScanner = true;
    this.logoQr = false;
    this.titleEscaner = "Escaneando";
  };

  public async scan(): Promise<void> {
    this.validaServicioSeguridad();

    try{
    const { barcodes } = await BarcodeScanner.scan({
      formats: [BarcodeFormat.QrCode],
    });

    let response = barcodes[0].rawValue;
    
    if(response != null && response != ""){
      /** Se valida fecha de caducidad **/
      let permiso = JSON.parse(decrypt(response));

      if(permiso.fecha == null || permiso.fecha == ""){
      /** valida residente **/
      this.visitasService.validaQr(permiso).subscribe(res => {
        if(res.response != null && res.code != null){
          Swal.fire({
            title: 'ALKI',
            icon: res.code,
            html: res.response
          })
        }else{
          Swal.fire({
            title: 'ALKI',
            icon: 'error',
            html: 'El QR no es válido.'
          })
        }
      });
      }else{
      /** valida visita **/
      this.visitasService.validaVigenciaQr(permiso.fecha).subscribe(res => {
        if(res.response == "Activo"){
          this.visitasService.validaQr(permiso).subscribe(res => {
            if(res != null){
              Swal.fire({
                title: 'ALKI',
                icon: res.code,
                html: res.response
              })
            }
          });
        }else{
          Swal.fire({
            title: 'ALKI',
            icon: 'info',
            html: 'El Código QR ha vencido.'
          })
        }
      });
      }//TerminaValidacion
    }
    }catch(error){
      console.log("Error al abrir el escaner: "+error);
    }
  }

  async share(canvas: HTMLCanvasElement){
    this.validaServicio();
  
    let base64 = canvas.toDataURL();
    let path = "qr.png";

    await Filesystem.writeFile({
      path,
      data: base64,
      directory: Directory.Cache
    }).then(async (res) =>{
      let uri = res.uri;
      await Share.share({
        url: uri,
      });

      await Filesystem.deleteFile({
        path,
        directory: Directory.Cache
      })
    })
  }

  guardar(){
    this.validaServicio();

    this.validaCantidad(this.acceso.cantidad);

    this.validaVisitante(this.acceso.nombre);

    if(parseInt(this.acceso.cantidad) > 5){
      Swal.fire({
        title: 'Ups!',
        text: 'La cantidad de visitas máximo permitido es de 5.',
        icon: 'info',
        showCancelButton: false,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Continuar!"
      }).then((result) => {
        if (result.isConfirmed) {
          this.acceso.cantidad = '';
        }else{
          this.acceso.cantidad = '';
        }
      });
    }
    
    if(parseInt(this.acceso.cantidad) == 0){
      Swal.fire({
        title: 'Ups!',
        text: 'La cantidad de visitas debe ser mayor a cero.',
        icon: 'info',
        showCancelButton: false,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Continuar!"
      }).then((result) => {
        if (result.isConfirmed) {
          this.acceso.cantidad = '';
        }else{
          this.acceso.cantidad = '';
        }
      });
    }

    if((this.acceso.tipo == null || this.acceso.tipo == "")){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El tipo de acceso es requerido.'
      })
    }

    if((this.acceso.cantidad == null || this.acceso.cantidad == "")){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'La cantidad de visitas es requerido.'
      })
    }
    
    if((this.acceso.nombre == null || this.acceso.nombre == "")){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El nombre de visita es requerido.'
      })
    }

    if((this.acceso.nombre != null && this.acceso.cantidad != null) && (this.acceso.nombre != "" && this.acceso.cantidad != "" && parseInt(this.acceso.cantidad) > 0) && parseInt(this.acceso.cantidad) <= 5){
      this.vigenciaQr();
      let tipoAcceso = (this.acceso.tipo == "Entrada") ? "v" : "s";
      this.acceso.token = encrypt(JSON.stringify({"nombre":this.acceso.nombre, "cantidad": parseInt(this.acceso.cantidad), "acceso":tipoAcceso, "id": this.idUsuario, "fecha": this.vigencia}));
      this.compartir = true;
      this.logoQr = false;
      this.acceso.nombre = '';
      this.acceso.cantidad = '';
      this.acceso.tipo = "";
    }
  }

  async initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res.correo != null && res.oaut != null){
        this.habilita = res.oaut;
        if(res.oaut){
          this.idUsuario = res.id;
        } else{
          this.requestPermissions();
        }
      }
    });
  }

  vigenciaQr(){
        this.visitasService.obtenerFechaActual().subscribe(res => {
          this.vigencia = res.response;
        });
  }

  /** valida servicio residente **/
  validaServicio(){
    this.servicioService.validaServicio(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }

  validaServicioSeguridad(){
    this.servicioService.validaServicioSeguridad(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }

}
