import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { NgIf } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, MatProgressSpinnerModule, NgIf],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  
  /** Se declara array user **/
  user = {
    correo: '',
    password: ''
  }

  /** Se declara visibilidad **/
  visibled = true;

  /** Se declara bandera spinner **/
  spinner = false;

  /** Se declara restaurarEmail **/
  formRecuperar = false;

  /** Se declara formLogin **/
  formLogin = true;

  /** Se declara titulo form y botones **/
  titleForm = 'Inicio de sesión';
  titleFormRecuperar = 'Recuperar cuenta';

  /** Se declaran botones **/
  textButtonRecuperar = "Enviar";
  textButtonLogin = 'Ingresar';

  constructor(private authService: AuthService, private router: Router, private networkService: NetworkService) { 
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
    if(this.authService.getToken()){
      this.visibled = false;
      this.router.navigate(['/home/inicio']);
    }
   }

  async ngOnInit(){
    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }
  }

  recuperar(){
    if(this.user.correo === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El correo electrónico es requerido'
      })
    }

    if(this.user.correo != ""){
      this.restaurarPassword(this.user);
      /** Se limpia campo **/
      this.user.correo = "";
    }
  }

  async login(){
    /** Se validan campos **/
    if(this.user.password === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'La contraseña es requerida'
      })
    }

    if(this.user.correo === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El correo electrónico es requerido'
      })
    }

    if(this.user.correo != "" && this.user.password != ""){
      if(this.user.password.length === 10){
      /** Se habilita spinner **/
      this.spinner = true;
      /** Se cambia mensaje **/
      this.titleForm = 'Cargando..';
      this.consultar(this.user);
      const isConnected = await this.networkService.checkNetworkStatus();
      if (!isConnected) {
        Swal.fire({
          title: "No tienes conexión a internet?",
          text: "Revisa tu conexión",
          icon: "question"
        });
        this.spinner = false;
        this.titleForm = '';
      }
      }else{
        Swal.fire({
          title: 'Ups!',
          icon: 'info',
          html: 'La contraseña debe tener 10 carácteres.'
        })
      }
    }
    
  }

  consultar(data : any){
    this.authService.login(this.user).subscribe(res => {

      if(res.token != undefined && res.token != null){
          localStorage.setItem('token',res.token);
          this.router.navigate(['/home/inicio']);
          /** Limpiar campos **/
          this.user.correo = "";
          this.user.password = "";
        }else{
          if(res.mensaje != null){
          Swal.fire({
            title: 'Ups!',
            icon: 'info',
            html: res.mensaje
          })
          }else{
          Swal.fire({
            title: 'Ups!',
            icon: 'info',
            html: 'Correo y/o contraseña incorrectos.'
          })
          }
          this.limpiar();
        }
    },
    (error) => {
      Swal.fire({
        title: 'Ups!',
        icon: 'error',
        html: 'Error al ingresar'
      })
      this.spinner = false;
      this.titleForm = '';
      }
    )
  }

  restaurarPassword(data : any){
    this.authService.restaurarPassword(this.user).subscribe(res => {
      if(res.status != null && res.mensaje != null){
          Swal.fire({
            title: "Cuenta restaurada",
            text: res.mensaje,
            icon: "success"
          });
          /** Limpiar campos **/
        }else{
          if(res.mensaje != null){
          Swal.fire({
            title: 'Ups!',
            icon: 'info',
            html: "Intente más tarde."
          })
          }
        }
    },
      err => console.log(err)
      )
  }

  restaurar(){
    this.formLogin = false;
    this.formRecuperar = true;
    this.user.correo = "";
  }

  iniciarSesion(){
    this.formLogin = true;
    this.formRecuperar = false;
    this.limpiar();
  }

  limpiar(){
    /** Limpiar formulario **/
    this.titleForm = 'Inicio de sesión';
    this.spinner = false;
    this.user.correo = "";
    this.user.password = "";
  }

}
