import { AfterViewInit, Component, OnInit  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import { AuthService } from '../../services/auth.service';
import { IndexService } from '../../services/index.service';
import { HomeService } from '../../services/home.service';
import {MatButtonModule} from '@angular/material/button';
import { QrCodeModule } from 'ng-qrcode';
import { ServicioService } from '../../services/servicio.service';
import { encrypt } from '../../util/util-encrypt';
import { VisitasService } from '../../services/visitas.service';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';

@Component({
  selector: 'app-index',
  standalone: true,
  imports: [FormsModule, MatButtonModule, QrCodeModule],
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],  
})

export class IndexComponent implements OnInit, AfterViewInit {

  user = {
    nombre: '',
    apellidos: '',
    correo: '',
    password: '',
    direccion: '',
    telefono: '',
    vencimiento: ''
  }

  habilita = false;
  idUsuario = 0;
  vigencia = "";

  acceso = {
    datos: ''
  }

  constructor(private authService : AuthService, private indexService: IndexService, private homeService: HomeService, private servicioService: ServicioService, private visitasService: VisitasService, private networkService: NetworkService) {
    this.initHome();
    this.initIndex();
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
  }

  cerrarSesion(){
    this.authService.logout();
  }

  async ngOnInit() {
    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }
  }

  ngAfterViewInit(): void {}

  guardar(){

    /** Se validan campos **/
    if(this.user.password === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'La contraseña es requerida'
      })
    }

    if(this.user.correo === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El correo electrónico es requerido'
      })
    }

    if(this.user.telefono === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El teléfono es requerido'
      })
    }

    if(this.user.apellidos === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'Los apellidos son requeridos'
      })
    }

    if(this.user.nombre === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El nombre es requerido'
      })
    }

    if(this.user.password !== "" && this.user.correo !== "" && this.user.telefono !== "" && this.user.apellidos !== "" && this.user.nombre !== ""){
      Swal.fire({
        title: "¿Desea guardar cambios?",
        text: "Revise su información antes de enviar!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Sí, enviar!",
        cancelButtonText: "Cancelar"
      }).then((result) => {
        if (result.isConfirmed) {
          this.indexService.indexUpdate(this.authService.getToken(), this.user).subscribe(res => {
            if(res!= null){
              Swal.fire({
                title: "ALKI",
                text: res.respuesta,
                icon: "success"
              });
            }else{
              Swal.fire({
                title: "ALKI",
                text: "Error al guardar!",
                icon: "error"
              });
            }
          });
        }
      });


    }

  }

  /** Init **/
  initIndex(){
    this.indexService.index(this.authService.getToken()).subscribe(res => {
      if(res != null){
        this.user.correo = res.correo;
        this.user.direccion = res.direccion;
        this.user.password = res.password;
        this.user.telefono = res.telefono;
        this.user.nombre = res.nombre;
        this.user.apellidos = res.apellidos;
        this.user.vencimiento = res.vigencia;
        this.crearQr();
      }
    });
  }

  initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res != null){
        this.habilita = res.oaut;
        if(res.oaut){
          this.idUsuario = res.id;
        }
        this.crearQr();
      }
    });
  }

  crearQr(){
    this.acceso.datos = encrypt(JSON.stringify({"nombre":this.user.nombre, "cantidad": 1, "acceso":"r", "id": this.idUsuario, "fecha": this.vigencia}));
  }

  vigenciaQr(){
    this.visitasService.obtenerFechaActual().subscribe(res => {
      this.vigencia = res.response;
    });
  }

  nuevo(){
    this.vigenciaQr();
    this.crearQr();
  }

}
