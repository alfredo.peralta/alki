import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesoSeguridadComponent } from './acceso-seguridad.component';

describe('AccesoSeguridadComponent', () => {
  let component: AccesoSeguridadComponent;
  let fixture: ComponentFixture<AccesoSeguridadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AccesoSeguridadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AccesoSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
