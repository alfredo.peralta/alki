import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import { AuthService } from '../../services/auth.service';
import { IndexService } from '../../services/index.service';
import { HomeService } from '../../services/home.service';
import { ServicioService } from '../../services/servicio.service';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { NetworkService } from '../../services/network.service';

@Component({
  selector: 'app-cuenta',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './cuenta.component.html',
  styleUrl: './cuenta.component.css'
})
export class CuentaComponent implements OnInit{

  user = {
    nombre: '',
    apellidos: '',
    correo: '',
    password: '',
    direccion: '',
    telefono: '',
    vencimiento: ''
  }

  habilita = false;

  constructor(private authService : AuthService, private indexService: IndexService, private homeService: HomeService, private servicioService: ServicioService, private networkService: NetworkService) {
    this.initHome();
    this.initIndex();
    // Bloquear la orientación a portrait
    ScreenOrientation.lock({ orientation: 'portrait' });
  }

  async ngOnInit(){
    const isConnected = await this.networkService.checkNetworkStatus();
    if (!isConnected) {
      Swal.fire({
        title: "No tienes conexión a internet?",
        text: "Revisa tu conexión",
        icon: "question"
      });
    }
  }

  guardar(){

    /** Se validan campos **/
    if(this.user.password.length < 10){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'La contraseña debe contener 10 carácteres'
      })
    }
    
    if(this.user.password === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'La contraseña es requerida'
      })
    }

    if(this.user.correo === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El correo electrónico es requerido'
      })
    }

    if(this.user.telefono === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El teléfono es requerido'
      })
    }

    if(this.user.apellidos === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'Los apellidos son requeridos'
      })
    }

    if(this.user.nombre === ""){
      Swal.fire({
        title: 'Ups!',
        icon: 'info',
        html: 'El nombre es requerido'
      })
    }

    if(this.user.password !== "" && this.user.correo !== "" && this.user.telefono !== "" && this.user.apellidos !== "" && this.user.nombre !== "" && this.user.password.length == 10){
      Swal.fire({
        title: "¿Desea guardar cambios?",
        text: "Revise su información antes de enviar!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Sí, enviar!",
        cancelButtonText: "Cancelar"
      }).then((result) => {
        if (result.isConfirmed) {
          this.indexService.indexUpdate(this.authService.getToken(), this.user).subscribe(res => {
            if(res!= null){
              Swal.fire({
                title: "ALKI",
                text: res.respuesta,
                icon: "success"
              });
            }else{
              Swal.fire({
                title: "ALKI",
                text: "Error al guardar!",
                icon: "error"
              });
            }
          });
        }
      });


    }

  }

  /** Init **/
  initIndex(){
    this.indexService.index(this.authService.getToken()).subscribe(res => {
      if(res != null){
        this.user.correo = res.correo;
        this.user.direccion = res.direccion;
        this.user.password = res.password;
        this.user.telefono = res.telefono;
        this.user.nombre = res.nombre;
        this.user.apellidos = res.apellidos;
        this.user.vencimiento = res.vigencia;
      }
    });
  }

  initHome(){
    this.homeService.init(this.authService.getToken()).subscribe(res => {
      if(res.correo != null && res.oaut != null){
        this.habilita = res.oaut;
        if(res.oaut){
          this.validaServicio();
        }else{
          this.validaServicioSeguridad();
        }
      }
    });
  }

  /** valida servicio residente **/
  validaServicio(){
    this.servicioService.validaServicio(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }

  validaServicioSeguridad(){
    this.servicioService.validaServicioSeguridad(this.authService.getToken()).subscribe(res => {
      if(res != null && res.code == "info"){
        Swal.fire({
          title: "ALKI",
          text: res.response,
          icon: res.code,
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continuar!"
        }).then((result) => {
          if (result.isConfirmed) {
            this.authService.logout();
          }else{
            this.authService.logout();
          }
        });
      }
    });
  }


}
