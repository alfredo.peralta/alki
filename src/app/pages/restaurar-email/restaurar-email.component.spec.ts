import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurarEmailComponent } from './restaurar-email.component';

describe('RestaurarEmailComponent', () => {
  let component: RestaurarEmailComponent;
  let fixture: ComponentFixture<RestaurarEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RestaurarEmailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RestaurarEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
