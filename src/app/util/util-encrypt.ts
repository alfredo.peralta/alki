import * as CrytoJS from 'crypto-js';

export const encrypt=(data:string):string => {
    return CrytoJS.AES.encrypt(data,'alki-security-200090365').toString();
}

export const decrypt=(value:string):string=>{
    const valueDecryot = CrytoJS.AES.decrypt(value,'alki-security-200090365').toString(CrytoJS.enc.Utf8);
    return valueDecryot;
}