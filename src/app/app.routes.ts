import { Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { authGuard } from './auth.guard';
import { RestaurarEmailComponent } from './pages/restaurar-email/restaurar-email.component';
import { ResidentesComponent } from './pages/residentes/residentes.component';
import { IndexComponent } from './pages/index/index.component';
import { AccesoResidenteComponent } from './pages/acceso-residente/acceso-residente.component';
import { AlertasComponent } from './pages/alertas/alertas.component';
import { CuentaComponent } from './pages/cuenta/cuenta.component';
import { VisitasComponent } from './pages/visitas/visitas.component';
import { AccesoSeguridadComponent } from './pages/acceso-seguridad/acceso-seguridad.component';

export const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'}, 
    {path: 'login', component: LoginComponent}, 
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [authGuard],
        children: [
            { path: 'inicio', component: IndexComponent, pathMatch: 'full'},
            {path: 'acceso-residente', component: AccesoResidenteComponent},
            {path: 'acceso-seguridad', component: AccesoSeguridadComponent},
            { path: 'residente', component: ResidentesComponent},
            {path: 'alertas', component: AlertasComponent},
            {path: 'cuenta', component: CuentaComponent},
            {path: 'visitas', component: VisitasComponent}
        ]
    },
    {path: 'restaurar', component: RestaurarEmailComponent}
];

export class AppRouter { } 
